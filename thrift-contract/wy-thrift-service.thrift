struct OrderInfo{
  1: string id,
  2: string type
}

exception message_error{
  1:i32 type
  2:string message
}

service WYThriftService {
  void setShopStatus(1: string shopId, 2: bool isOnline) throws(1: message_error error)
  OrderInfo checkUnhandledOrder(1: string shopId)  throws(1:message_error error)
}