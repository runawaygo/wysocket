#!/bin/sh

env=$1

if [ "$env" = "production" ]; then 
  server="112.124.27.27"
elif [ "$env" = "integration" ]; then 
  server="121.40.115.13"
else 
  echo "sh deploy.sh production|integration" 
  exit
fi 

echo Env: $env, Server: $server

rm -rf wy.tar.gz
tar  --exclude node_modules/ -zcvf wy.tar.gz *

scp wy.tar.gz root@$server:~
ssh root@$server 'mkdir -p wy & tar -xvzf wy.tar.gz -C wy'

ssh root@$server "
  cd wy
  sudo cp -Rf system-config/autostart_wy /etc/init.d/wy
  sudo chmod +x /etc/init.d/wy
  sudo update-rc.d wy defaults 

  sh system-config/tcp.sh

  coffee -c .
  
  pushd socket-server
  npm install
  popd

  pushd socket-client
  npm install
  popd


  pushd mock-api-server
  npm install
  popd

  pushd admin-web
  cp -Rf config/admin.$env.json config/admin.json
  npm install
  popd

  pkill node
  coffee -bc .
  sh start.sh $env
"