#!/bin/sh

pkill node
echo "Start by env: $1"
env=$1

/usr/local/bin/node /usr/local/bin/forever start /root/wy/admin-web/app.js
NODE_ENV=$env /usr/local/bin/node /usr/local/bin/pomelo start -d /root/wy/socket-server -e $env -D

date >> socket-server/logs/restart.log