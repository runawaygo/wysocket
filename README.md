# 我有外卖SOCKET  
### 说明
我有外卖的Socket服务器，主要用于高并发地同外卖机进行通讯。  
项目主要采用了 [Pomelo框架](https://github.com/NetEase/pomelo) 实施


### 维护  
#### 重新部署
测试环境 sh deploy.sh integration  
生产环境 sh deploy.sh production

#### 线上维护
使用 pomelo list 来查看现在的节点启动情况  
使用 forever list 来查看admin－web的情况  
重启 sh start.sh integration/production

#### 业务查询
可以通过admin-web来查看目前链接到各个节点上面的人数  
将店铺SHDP001的按照时间排序的所有行为统计到文件中便于详细查询 cat *.log | grep SHDP001 | sort > SHDP001.log

### 安装部署
1. 将项目安装到目标服务器(linux)  
*  执行pre-install.sh，进行依赖组建安装  
*  在socket-server， admin-web文件夹中分别执行npm install  
*  cp -Rf admin-web/config/admin.integration.json admin-web/config/admin.json，并修改admin.json中的服务器地址为socket-server的部署地址  
*  修改socket-server/config/servers.json当中的配置，决定节点和服务数量  
*  修改socket-server/config/master.json当中的配置，决定master节点的启动服务器(一般调节为线上服务器的内网地址)  
*  修改socket-server/config/urlConfig.coffee当中的配置，决定依赖的下层服务地址  
*  修改socket-server/config/redisConfig.coffee当中的配置，决定以来的redis地址  
*  执行start.sh integration/production启动服务  
*  更多细节可参考[Pomelo框架](https://github.com/NetEase/pomelo/wiki/Home-in-Chinese)  