net = require("net")

count = process.env.count or 10000
host = process.env.host
port = process.env.port or 8124

diffArray = []
map = {}
max = 0

connectionCount = 0

go = (host, port)->
  client = net.connect(
    port: port
    host: host
  , -> #'connect' listener
    connectionCount++
    client.write "HB:01234567890123456789"

    setInterval ->
      start = Date.now()
      map[start] = start
      client.write "RQ:#{start}-012345678901234567890123456789"
      ticker = Date.now()
    ,5*1000

  )

  client.on "data", (chunk)->
    start = parseInt chunk.toString("utf8", 3,16)
    diff = Date.now()-start
    if diff > max
      max = diff

    diffArray.push(diff)


  client.on "end", -> 
    console.log "end"
    connectionCount--

  client.on "error", (error)-> 
    console.log "error", error

if process.env.local
  for i in [0..count]
    setTimeout ->
      # go("10.165.63.88", 8124)
      go("121.42.56.205", port)

    , i*20/count*1000

else 
  for i in [0..count]
    setTimeout ->
      go(host, port)
      # go("121.42.56.205", 8124)

    , i*20/count*1000
    
setTimeout ->
  setInterval ->
    sum = 0
    len = diffArray.length
    diffArray.forEach (item)-> sum+= item
    avg = sum/len
    diffArray = []

    console.log("pid:#{process.pid},avg:#{avg}, max:#{max}, msg:#{len}, con:#{connectionCount}, \ntime:#{new Date()}")
    max = 0

  ,5000
,(5 - Date.now()/1000%5)*1000
