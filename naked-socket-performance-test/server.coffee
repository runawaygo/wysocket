net = require("net")

responseData = ([0..30]).map(->"0123456789").join("")

connections = {}
count = 0

server = net.createServer (c) -> 
  c.id = count++
  connections[c.id] = true
  
  c.on "end", ->
    if connections[c.id]
      delete connections[c.id]

    console.log "server end"
  c.on "error", (err)->
    if connections[c.id]
      delete connections[c.id]

    console.log err

  c.on "data", (chunk)->
    type = chunk.toString("utf8", 0,2)
    
    switch type
      when "HB"
        handletype = "HB"
      when "RQ"
        c.write(chunk+responseData)

port = process.env.port or 8124

server.listen port, -> #'listening' listener
  console.log "server bound"
  process.stdin.resume()

setInterval ->
  console.log "count:#{Object.keys(connections).length}, \ntime:#{new Date()}"
,3000

