redisFactory = require("../socket-server/app/utils/redisFactory")
redisConfig = require("../socket-server/config/redisConfig")

count = parseInt(process.env.count) or 6000
duration =parseInt(process.env.duration) or 30
balance = process.env.host or "http://10.168.79.114:3014"
base = parseInt(process.env.base) or 0

dataCount = 0
dataBefore = 0
dataAvg = 0
client = redisFactory.createClient()
setInterval ->
  dataAvg = (dataCount - dataBefore)/3
  dataBefore = dataCount 
  console.log "dataCount: #{dataCount}, dataAvg: #{dataAvg}"
, 3000

client.on "ready", ->
  setInterval ->
    baseIndex = 100000 + 1*count+1
    for i in [0...count]
      index = baseIndex
      shopId = "S#{index}"
      lazyPush(shopId, duration*1000/count*i)
  , 5000

lazyPush = (shopId, duration)->
  setTimeout ->
    order = 
      id: 1234
      type: "OK"
      shopId: shopId
    dataCount++
    client.publish redisConfig.orderUpdateChannelName, JSON.stringify(order)
  , duration