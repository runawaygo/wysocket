// Generated by CoffeeScript 1.8.0
var balance, base, client, count, dataAvg, dataBefore, dataCount, duration, lazyPush, redisConfig, redisFactory;

redisFactory = require("../socket-server/app/utils/redisFactory");

redisConfig = require("../socket-server/config/redisConfig");

count = parseInt(process.env.count) || 6000;

duration = parseInt(process.env.duration) || 30;

balance = process.env.host || "http://10.168.79.114:3014";

base = parseInt(process.env.base) || 0;

dataCount = 0;

dataBefore = 0;

dataAvg = 0;

client = redisFactory.createClient();

setInterval(function() {
  dataAvg = (dataCount - dataBefore) / 3;
  dataBefore = dataCount;
  return console.log("dataCount: " + dataCount + ", dataAvg: " + dataAvg);
}, 3000);

client.on("ready", function() {
  return setInterval(function() {
    var baseIndex, i, index, shopId, _i, _results;
    baseIndex = 100000 + 1 * count + 1;
    _results = [];
    for (i = _i = 0; 0 <= count ? _i < count : _i > count; i = 0 <= count ? ++_i : --_i) {
      index = baseIndex;
      shopId = "S" + index;
      _results.push(lazyPush(shopId, duration * 1000 / count * i));
    }
    return _results;
  }, 5000);
});

lazyPush = function(shopId, duration) {
  return setTimeout(function() {
    var order;
    order = {
      id: 1234,
      type: "OK",
      shopId: shopId
    };
    dataCount++;
    return client.publish(redisConfig.orderUpdateChannelName, JSON.stringify(order));
  }, duration);
};
