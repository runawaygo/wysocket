process.env.NODE_ENV ?= "development"

thrift = require 'thrift'
service = require "../thrift-contract/gen-nodejs/WYThriftService"
ttypes = require "../thrift-contract/gen-nodejs/wy-thrift-service_types"

module.exports = app = (callback) ->
  server = thrift.createServer service,
    setShopStatus:(shopId, isOnline, callback)->
      console.log "setShopStatus", shopId, isOnline
      callback()

    checkUnhandledOrder:(shopId, callback)->
      console.log "checkUnhandledOrder", shopId
      orderInfo = new ttypes.OrderInfo {id:"superowlf" , type: "OK"}
      callback null, orderInfo
      
  , {}

  port = process.env.PORT or 5001
  server.listen port, () ->
    console.log "server listen port #{port}"
    callback?()

app() unless module.parent

