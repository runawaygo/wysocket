express = require('express')
bodyParser = require('body-parser')

app = express()
app.use(bodyParser())

app.post '/checkshopexist',  (req, res)->
  console.log "checkshopexist", req.body
  res.send({code: 1, msg:"success"})

app.post '/setShopState',  (req, res)->
  console.log "setShopState", req.body
  res.send({code: 1, msg:"success"})

app.post '/queryUnhandledOrder',  (req, res)->
  console.log "queryUnhandledOrder", req.body
  res.json {code: 1, list: [{id:"superowlf" , type: "OK"}, {id:"superowlf" , type: "CL"}]}

server = app.listen 5000, ->

  host = server.address().address
  port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)