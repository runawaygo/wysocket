redisFactory = require("../socket-server/app/utils/redisFactory")
redisConfig = require("../socket-server/config/redisConfig")

shopId = process.env.shopId or "S100010"

dataCount = 0
dataBefore = 0
dataAvg = 0
client = redisFactory.createClient()
setInterval ->
  dataAvg = (dataCount - dataBefore)/3
  dataBefore = dataCount 
  console.log "dataCount: #{dataCount}, dataAvg: #{dataAvg}"
, 3000

client.on "ready", ->
  setInterval ->
    order = 
      orderId: 1234
      type: "1"
      shopId: shopId
    
    client.publish redisConfig.orderUpdateChannelName, JSON.stringify(order), (err)->
      console.log err if err
      dataCount++       

  , 3000
