config = 
  development:
    server:'127.0.0.1'
    port:6379
    log: true
    maxRedisConnection: 20
    minRedisConnection: 10

    orderUpdateChannelName: "order_update_channel"  

  integration:
    server:'r-bp129d65cd571ed4.redis.rds.aliyuncs.com'
    port:6379
    log: false
    maxRedisConnection: 50
    minRedisConnection: 20
    auth_pass: "M3FHp65w"
    
    orderUpdateChannelName: "order_update_channel"

  production:
    server:'r-bp1ed0e9d8f200e4.redis.rds.aliyuncs.com'
    port:6379
    log: false
    maxRedisConnection: 50
    minRedisConnection: 20
    auth_pass: "914fE5c0"
    
    orderUpdateChannelName: "order_update_channel"


env = process.env.NODE_ENV ? "production"
module.exports = config[env]