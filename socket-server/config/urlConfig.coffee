urls = 
  development:
    order: "http://127.0.0.1:5000"
    shop: "http://127.0.0.1:5000"
  integration:
    order: "http://10.171.204.70:207"
    shop: "http://10.171.204.70:207"
  production:
    order: "http://10.242.173.186:207"
    shop: "http://10.242.173.186:207"
  # production:
  #   order: "http://10.160.10.195:5000"
  #   shop: "http://10.160.10.195:5000"

env = process.env.NODE_ENV ? "production"

module.exports = urls[env]