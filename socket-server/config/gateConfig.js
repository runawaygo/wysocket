// Generated by CoffeeScript 1.10.0
(function() {
  var config, env, ref;

  config = {
    development: {
      port: 3014
    },
    integration: {
      port: 3014
    },
    production: {
      port: 3014
    }
  };

  env = (ref = process.env.NODE_ENV) != null ? ref : "production";

  module.exports = config[env];

}).call(this);
