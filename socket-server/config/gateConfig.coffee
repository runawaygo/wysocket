config = 
  development:
    port: 3014

  integration:
    port: 3014
    
  production:
    port: 3014

env = process.env.NODE_ENV ? "production"
module.exports = config[env]