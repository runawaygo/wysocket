redis = require 'redis'
redisCommands = require 'redis/lib/commands'
poolModule = require 'generic-pool'
config = require "../../config/redisConfig"

redisPoolClient = null

module.exports = 
  createClient: ->
    client = redis.createClient config.port, config.server, config
    client


  getPooledClient: ->
    unless redisPoolClient
      redisPoolClient = createWrapperedPoolClient(config)
    redisPoolClient


generateRedisPool = (config)->
  pool = poolModule.Pool
    name: "redis"
    create: (callback) ->
      cachedClient = redis.createClient config.port, config.server, config

      cachedClient.on "error", (err) -> logger.error "redis error: #{err}"

      callback null, cachedClient

    destroy: (cachedClient) ->
      cachedClient.end()

    max: config.maxRedisConnection
    # optional. if you set this, make sure to drain() (see step 3)
    min: config.minRedisConnection
    # specifies how long a resource can stay idle in pool before being removed
    idleTimeoutMillis: config.idleTimeoutMillis ? 30000
    # if true, logs via console.log - can also be a function
    log: config.log ? false 

  pool

createWrapperedPoolClient = (config)->
  pooledClient = {}
  pool = null
  for cmd in redisCommands
    pooledClient[cmd] = ((cmd)->
      unless pool
        pool = generateRedisPool(config)
        throw new Error('Create redis pool failed!!!') unless pool      

      ->
        # throw 'redis command must be called with at least on argument!' if arguments.length is 0
        args = Array.prototype.slice.call arguments
        argsLength = args.length
        pool.acquire (err, client) ->
          if err
            logger.error "acquire redis connection error: #{err}"
            return

          lastArg = args[args.length-1]
          lastArgType = typeof lastArg
          if lastArgType is 'function'
            _callback = lastArg
          else if lastArgType is 'undefined'
            _callback = ->
          else
            _callback = ->
            args.push _callback
          callback = ->
            pool.release client
            _callback.apply null, arguments
          if argsLength is 0 then args.push callback else args[args.length-1] = callback

          client[cmd].apply client, args

    )(cmd)
  pooledClient