client = require 'superagent'
serviceConfig = require './restConfig'
url = require 'url'
path = require 'path'

logger = require("pomelo-logger").getLogger("customer-log", __filename)
service = {}

generateService = (item) ->
  (options, callback) ->
    _args = options.args ? {}
    _extendPath = options.extendPath ? ""
    _replacePath = options.replacePath ? {}
    _callback = callback ? (errr, res) ->
    cb = (error, res) ->
      if error
        _callback error, null
      else if res.error
        _callback res.error, null
      else
        _callback null, res
    
    path = url.resolve item.path, _extendPath
    for key, value of _replacePath
      path = path.replace ":#{key}", value 

    if process.env.NODE_ENV isnt "production"
      console.log path + '?' + JSON.stringify(_args)

    logger.info "http url:#{path}, method:#{item.method}, args:#{JSON.stringify(_args)}"
      
    switch item.method.toUpperCase()
      when "POST"
        contentType = if item.contentType then item.contentType else 'form'
        client.post(path)
          .type(contentType)
          .send(_args)
          .end (err, res)->
            if err
              logger.error "http url error:#{path},  args:#{JSON.stringify(_args)}, statusCode: #{res?.statusCode}, error:#{err}, body: #{JSON.stringify(res.body)}"

            else if res.body?.code isnt 1
              logger.error "http url error:#{path},  args:#{JSON.stringify(_args)}, statusCode: #{res?.statusCode}, error:#{err}, body: #{JSON.stringify(res.body)}"
              err = 400 

            else
              logger.info "http url success:#{path},  args:#{JSON.stringify(_args)}, body: #{JSON.stringify(res.body)}"

            cb err, res
          
      when "PUT"
        client.put(path)
          .send(_args)
          .end cb

      when "DELETE"
        client.del(path)
          .send(_args)
          .end cb

      else
        client.get(path)
          .query(_args)
          .end cb
      

for name, moduleConfig of serviceConfig
  s = service[name] = {}
  for name, item of moduleConfig
    s[name] = generateService(item)

module.exports = service