module.exports.stateToRoute = (state)->
  switch parseInt(state)
    when 1
      "orderUpdate.orderHandler.orderUpdate"
    when 2
      "orderUpdate.orderHandler.orderCancel"
    else
      null

module.exports.routeToState = (route)->
  switch route
    when "orderUpdate.orderHandler.orderUpdate"
      "1"
    when "orderUpdate.orderHandler.orderCancel"
      "2"
    else
      null

module.exports.heartbeatToState = (route)->
  switch route
    when "S"
      "1"
    when "C"
      "2"
    when "B"
      "3"
    else
      null