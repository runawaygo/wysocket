thrift = require('thrift')
config = require "../../config/thriftConfig"
service = require "../../../thrift-contract/gen-nodejs/WYThriftService"


client = null

module.exports = (callback)->
  hasReturned = false
  connection = thrift.createConnection config.domain, config.port, {transport:thrift[config.transport]}

  client = thrift.createClient service, connection

  connection.once 'connect', -> 
    hasReturned = true
    callback(null, client)

  connection.on 'error', (err)-> 
    client = null
    callback err unless hasReturned