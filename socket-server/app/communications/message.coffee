messageType = 
  HEARTBEAT: "HP"
  HEARTBEAT_ACK: "HR"

  SHOP_LOGIN: "DP"
  SHOP_LOGIN_ACK: "DR"

  ORDER_UPDATE: "OK"

  ORDER_CANCEL: "CL"
  ORDER_CANCEL_ACK: "DC"
  ORDER_CANCEL_NOT_PUSH: "CT"

  DISCONNECT: "ED"

messageTypeMapEvent = {}
messageTypeMapEvent[messageType.HEARTBEAT] = "message"
messageTypeMapEvent[messageType.SHOP_LOGIN] = "message"
messageTypeMapEvent[messageType.ORDER_CANCEL_ACK] = "message"
messageTypeMapEvent[messageType.DISCONNECT] = "message"

messageTypeMapRouter = {}
messageTypeMapRouter[messageType.SHOP_LOGIN] = "connector.shopHandler.shopLogin"
messageTypeMapRouter[messageType.HEARTBEAT] = "connector.shopHandler.heartbeat"

messageTypeMapRouter[messageType.ORDER_CANCEL] = "connector.orderHandler.orderCancel"
messageTypeMapRouter[messageType.ORDER_CANCEL_ACK] = "connector.orderHandler.orderCancelAck"

messageTypeMapRouter[messageType.DISCONNECT] = "connector.shopHandler.offline"


routerMapMessageType = 
  "connector.shopHandler.shopLogin": messageType.SHOP_LOGIN_ACK
  "connector.shopHandler.heartbeat": messageType.HEARTBEAT_ACK

  "orderUpdate.orderHandler.orderCancel": messageType.ORDER_CANCEL
  "connector.orderHandler.orderCancelAck": messageType.ORDER_CANCEL_NOT_PUSH
  
  "orderUpdate.orderHandler.orderUpdate": messageType.ORDER_UPDATE

  
module.exports = 
  messageTypeMapEvent: messageTypeMapEvent
  messageTypeMapRouter: messageTypeMapRouter
  routerMapMessageType: routerMapMessageType
  messageType: messageType

