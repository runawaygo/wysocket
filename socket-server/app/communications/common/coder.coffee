logger = require("pomelo-logger").getLogger("pomelo", __filename)
messageTypeMapRouter = require("../message").messageTypeMapRouter
routerMapMessageType = require("../message").routerMapMessageType

encode = (reqId, route, msg) ->
  type = routerMapMessageType[route]
  
  return new Buffer("#{type}:#{msg.content}&")

decode = (buffer) ->
  type = buffer.toString("utf8", 0, 2)
  data = buffer.toString("utf8", 3)

  msg = 
    id: Date.now()%100
    route: messageTypeMapRouter[type]
    body: 
      # shop: buffer.toString("utf8", 3, 10)
      # content: buffer.toString("utf8", 10, buffer.length-1)
      content: data.substring(0, data.indexOf('&'))
  
  msg

module.exports =
  encode: encode
  decode: decode
