# Generated by CoffeeScript 1.8.0
util = require("util")
EventEmitter = require("events").EventEmitter
net = require("net")
WySocket = require("./wySocket")
# Heartbeat = require("./commands/heartbeat")
coder = require("./common/coder")
curId = 1

###
Connector that manager low level connection and protocol bewteen server and client.
Develper can provide their own connector to switch the low level prototol, such as tcp or probuf.
###
Connector = (port, host, opts) ->
  return new Connector(port, host, opts)  unless this instanceof Connector
  EventEmitter.call this
  @opts = opts or {}
  @port = port
  @host = host
  return

util.inherits Connector, EventEmitter
module.exports = Connector

###
Start connector to listen the specified port
###
Connector::start = (cb) ->
  app = require("pomelo").app
  gensocket = (socket) =>
    wySocket = new WySocket(curId++, socket, @opts)
    @emit "connection", wySocket
    
  @tcpServer = net.createServer()
  @tcpServer.listen(@port)

  @tcpServer.on "connection", (socket)->
    gensocket socket

  process.nextTick cb

Connector::stop = (force, cb) ->
  @tcpServer.close()

  process.nextTick cb

Connector.decode = Connector::decode = coder.decode
Connector.encode = Connector::encode = coder.encode
