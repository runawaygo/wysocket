logger = require("pomelo-logger").getLogger("customer-log", __filename)
express = require("express")
bodyParser = require("body-parser")
dispatch = require("../utils/dispatcher").dispatch
gateConfig = require("../../config/gateConfig")

module.exports = (app, opts)-> new Balance(app, opts)

class Balance
  constructor:(@app, @opts)->

  start: (cb)->
    webapp = express()
    webapp.use(bodyParser())

    webapp.get '/:shopId',  (req, res)=>
      shopId = req.params.shopId

      connectorServers = @app.getServersByType('connector')
      targetServer = dispatch(shopId, connectorServers)
      logger.info "Balance: ", shopId, targetServer
      res.json targetServer

    @webserver = webapp.listen gateConfig.port, =>
      host = @webserver.address().address
      port = @webserver.address().port

      logger.info('Balance web server listening at http://%s:%s', host, port)
      process.nextTick cb

  afterStart: (cb)->
    process.nextTick cb

  stop: (cb)->
    @webserver.close()
    process.nextTick cb
