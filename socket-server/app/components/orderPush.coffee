logger = require("pomelo-logger").getLogger("customer-log", __filename)
ShopPushSchedule = require("./shopPushSchedule")
redisFactory = require("../utils/redisFactory")
redisConfig = require("../../config/redisConfig")
heapdump = require("heapdump")
dispatch = require("../utils/dispatcher").dispatch

module.exports = (app, opts)-> new OrderPush(app, opts)

class OrderPush
  constructor:(@app, @opts)->
    @shopPushScheduleCollection = {}
    @shopPushScheduleCancelOrderCollection = {}
    @orderLog = {}
    @app.set("orderLog", @orderLog)

  start: (cb)->
    # setInterval ->
    #   console.log(__dirname + "/" + Date.now() + '.heapsnapshot')
    #   heapdump.writeSnapshot(__dirname + "/" + Date.now() + '.heapsnapshot');
    # ,10000

    client = redisFactory.createClient()
    client.on "error", (err)-> 
      #Send email
      logger.error 'redis error: ', err, err.stack
      throw err

    client.on 'end', (err)=>
      return if err 
      
      logger.error 'redis end: '


    client.on 'message', (channel, message)=>
      logger.info 'order push message: ', message
      ###
      message:
      {
        shopId: {shopId}
        type: {type}   OK:新订单 CL:取消订单
        orderId: {orderId} 
      }
      ###
      order = JSON.parse(message)
      if not order.shopId or not order.type or not order.orderId
        logger.warn "Couldn't handle the message: #{message}"
        return
        
      @orderLog[order.shopId] = order
      @pushOrder(order.shopId, order)

    client.on 'ready', (err)->
      if err
        logger.error 'Redis message error: ', err, err.stack 
        throw err

      client.subscribe(redisConfig.orderUpdateChannelName)
      process.nextTick cb

  pushOrder:(shop, order)->
    collection = if order.type is "1" then @shopPushScheduleCollection else @shopPushScheduleCancelOrderCollection

    unless collection[shop]
      collection[shop] = new ShopPushSchedule(shop, @app, @opts)

    collection[shop].pushOrder order

  afterStart: (cb)->
    process.nextTick cb

  stop: (cb)->
    process.nextTick cb
