logger = require("pomelo-logger").getLogger("customer-log", __filename)
webservice = require("../utils/webservice")

module.exports = (app, opts)-> new StateService(app, opts)

class StateService
  constructor:(@app, @opts)->
    @stateCollection = {}

  setState: (shopId, shopState, callback)->
    return if @stateCollection[shopId] is shopState
    options = 
      args:
        params: JSON.stringify
          shopId: shopId
          state: shopState

    webservice.shop.setShopState options, (err, res)=>
      callback err if callback

      if err
        logger.error "Error: reqeust webservice.shop.setShopStatus failed! options: #{options.args.params}, body:#{JSON.stringify(res.body)}, err: #{err}"
        return 

      @stateCollection[shopId] = shopState

  resetState: (shopId)->
    @stateCollection[shopId] = null