logger = require("pomelo-logger").getLogger("customer-log", __filename)
webservice = require("../utils/webservice")

module.exports = (app, opts)-> new OfflineSchedule(app, opts)

class OfflineSchedule
  constructor:(@app, @opts)->
    @scheduleCollection = {}

  offline: (shopId, immediately, cb)->
    if @scheduleCollection[shopId]
      clearTimeout(@scheduleCollection[shopId])

    twoMins = 2 * 60 * 1000

    duration = if immediately then 0 else twoMins

    @scheduleCollection[shopId] = setTimeout =>
      options = 
        args:
          params: JSON.stringify
            shopId: shopId
            state: "2"

      logger.info("offline: ShopId: #{shopId} http request ")
      webservice.shop.setShopState options, (err, res)=>
        if err
          logger.error "Error: reqeust webservice.shop.setShopStatus failed! options: #{options.args.params}, body:#{JSON.stringify(res.body)}, err: #{err}"
          return 

        @scheduleCollection[shopId] = null

    , duration

    process.nextTick cb if cb

  online: (shopId, cb)->
    logger.info("online: ShopId: #{shopId} ")
    if @scheduleCollection[shopId]
      clearTimeout(@scheduleCollection[shopId])
      @scheduleCollection[shopId] = null
      process.nextTick cb if cb