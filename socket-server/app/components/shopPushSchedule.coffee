logger = require("pomelo-logger").getLogger("customer-log", __filename)
dispatch = require("../utils/dispatcher").dispatch
express = require('express')
bodyParser = require('body-parser')
state = require("../utils/state")

class ShopPushSchedule
  constructor:(@shop, @app, @opts)->
    @messageList = []
    connectorServers = @app.getServersByType('connector')
    @hitServer = dispatch(@shop, connectorServers)
    @lastPush = Date.now() - @delayDuration * 2
    @lastOrder = null
    @ticking = false

  delayDuration: 8000

  pushOrder:(order)->
    # return if @hitServer.id isnt @app.serverId
    logger.info "pushOrder: ", JSON.stringify(order)
    
    @lastOrder = order
    
    return if @ticking

    past = Date.now() - @lastPush
    if past > @delayDuration
      @_pushOrder()
    else
      setTimeout @_pushOrder.bind(@) ,@delayDuration - past  
      @ticking = true

  _pushOrder:->
    logger.info "_pushOrder", JSON.stringify({shop:@shop, order:@lastOrder ,server:@hitServer})
    route = state.stateToRoute(@lastOrder.type)
    
    @app
      .get('channelService')
      .pushMessageByUids(
        route
        {shop:@shop, content: @lastOrder.orderId}
        [{uid:@shop, sid:@app.serverId}]
        {}
        (err)-> logger.error err if err
      )

    @lastPush = Date.now()
    @ticking = false

  
module.exports = ShopPushSchedule