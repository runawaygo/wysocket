#!
# * Pomelo -- consoleModule onlineUser 
# * Copyright(c) 2012 Guan Huang <huangguan3112789@gmail.com>
# * MIT Licensed
# 
logger = require("pomelo-logger").getLogger(__filename)
module.exports = (opts) ->
  new Module(opts)

module.exports.moduleId = "onlineUser"
Module = (opts) ->
  opts = opts or {}
  @app = opts.app
  @type = opts.type or "pull"
  @interval = opts.interval or 1
  return

Module::monitorHandler = (agent, msg) ->
  connectionService = @app.components.__connection__
  if connectionService
    connectionServiceInfo = connectionService.getStatisticsInfo()
    statisticsInfo = 
      serverId: connectionServiceInfo.serverId
      totalConnCount: connectionServiceInfo.totalConnCount
      loginedCount: connectionServiceInfo.loginedCount

    agent.notify module.exports.moduleId, statisticsInfo
    return

Module::masterHandler = (agent, msg) ->
  unless msg
    # pull interval callback
    agent.notifyByType "connector", module.exports.moduleId
    return

  data = agent.get(module.exports.moduleId)
  unless data
    data = {}
    agent.set module.exports.moduleId, data

  data[msg.serverId] = msg

Module::clientHandler = (agent, msg, cb) ->
  cb? null, agent.get(module.exports.moduleId)
  return
