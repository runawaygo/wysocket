logger = require("pomelo-logger").getLogger("pomelo", __filename)

module.exports = (app) ->
  new Handler(app)

Handler = (app) ->
  @app = app
  return


###
New client entry.

@param  {Object}   msg     request message
@param  {Object}   session current session object
@param  {Function} next    next step callback
@return {Void}
###
Handler::orderCancelAck = (msg, session, next) ->  
  next null, msg

