logger = require("pomelo-logger").getLogger("customer-log", __filename)
hbLogger = require("pomelo-logger").getLogger("heartbeat-log", __filename)
dispatch = require("../../../utils/dispatcher").dispatch
webservice = require("../../../utils/webservice")
state = require("../../../utils/state")

Handler = (app) ->
  @app = app
  return


###
New client entry.

@param  {Object}   msg     request message
@param  {Object}   session current session object
@param  {Function} next    next step callback
@return {Void}
###
Handler::shopLogin = (msg, session, next) ->
  shopId = msg.content

  options = 
    args:
      params: JSON.stringify
        shopId: shopId

  #检查店铺是否存在
  # request.post("http://127.0.0.1:5000/checkShopExist").type('form').send(options.args).end (err, res)=>  
  
  webservice.shop.checkShopExist options, (err, res)=>

    backendSessionService = @app.get("sessionService")

    if err
      logger.error "Error: reqeust webservice.shop.checkShopExist failed! options: #{options.args.params}, body:#{JSON.stringify(res.body)}, err: #{err}"
      backendSessionService.kickBySessionId(session.id, "no match shop")
      return

    offlineSchedule = @app.get("offlineSchedule")
    offlineSchedule.online(shopId)

    #kick the old connect
    connectorServers = @app.getServersByType('connector')
    targetServer = dispatch(shopId, connectorServers)
    backendSessionService.kick shopId, "Another #{shopId} login", =>
      session.bind(shopId)
      next null, msg

      #检查店铺是否有未处理的订单
      options = 
        args:
          params: JSON.stringify
            shopId: shopId

      webservice.shop.queryUnhandledOrder options, (err, res)=>  
        logger.info "queryUnhandledOrder:#{shopId}", err, res.text
        if err
          logger.error err
          return

        if res.body?.code isnt 1
          logger.error res.body.msg
          return

        return unless res.body?.list
        
        for order in res.body?.list
          route = state.stateToRoute(order.state)

          continue unless route
          
          @app
            .get('channelService')
            .pushMessageByUids(
              route
              {shop:session.uid, content: order.oId}
              [{uid:session.uid, sid:session.frontendId}]
              (err)-> 
                logger.error err if err
            )




###
New client entry.

@param  {Object}   msg     request message
@param  {Object}   session current session object
@param  {Function} next    next step callback
@return {Void}
###

stateReg = /\.(.)\./
Handler::heartbeat = (msg, session, next) ->
  hbLogger.info("heartbeat: #{session.uid}, #{msg.content}")

  matches = stateReg.exec msg.content
  return if matches.length < 2
  shopState =  state.heartbeatToState(matches[1])

  stateService = @app.get("stateService")
  stateService.setState(session.uid, shopState)


###
New client entry.

@param  {Object}   msg     request message
@param  {Object}   session current session object
@param  {Function} next    next step callback
@return {Void}
###
Handler::offline = (msg, session, next) ->
  logger.info "offlineSchedule", session.uid  
  unless session.uid
    next null, msg
    return

  logger.info "offlineSchedule", session.uid
  
  # options = 
  #   args:
  #     params: JSON.stringify
  #       shopId: session.uid
  #       state: 2

  # webservice.shop.setShopState options, (err, res)=>
  #   if err
  #     logger.error "Error: reqeust webservice.shop.setShopState failed!"
  #     logger.error err 

  stateService = @app.get("stateService")
  stateService.resetState(session.uid)

  offlineSchedule = @app.get("offlineSchedule")
  offlineSchedule.offline(session.uid)

  session.unbind()

module.exports = (app) ->
  new Handler(app)