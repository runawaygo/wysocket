pomelo = require("pomelo")
logger = require("pomelo-logger")

connector = require("./app/communications/connector")
orderPush = require('./app/components/orderPush')
OfflineSchedule = require('./app/components/offlineSchedule')
StateService = require('./app/components/stateService')
balance = require('./app/components/balance')
onlineUser = require('./app/modules/onlineUser')


###
Init app for client.
###
app = pomelo.createApp()
app.set "name", "WYSocketCluster"

# app configuration
app.configure "production|integration|development", ->
  app.enable('systemMonitor')
  app.enable('rpcDebugLog')
  app.registerAdmin(onlineUser, {app:app})

  serverId = app.getServerId()
  base = app.getBase()
  logger.configure("#{__dirname}/config/log4js.json", {serverId: serverId, base: base})
  # app.filter(pomelo.timeout())

app.configure "production|integration|development", "connector", ->    
  app.load(orderPush)
  app.set("offlineSchedule", OfflineSchedule(app, {}))
  app.set("stateService", StateService(app, {}))
  
  app.set "connectorConfig",
    connector: (port, host, opts) ->
      new connector(port, host,
        heartbeat: 8 * 1000
      )

# app.configure "production|development", "orderPush", ->
#   app.load(orderPush)

app.configure "production|integration|development", "balance", ->
  app.load(balance)


# start app
app.start()
process.on "uncaughtException", (err) ->
  console.error " Caught exception: " + err.stack
  return