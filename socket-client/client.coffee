
net = require("net")
request = require("superagent")

count = parseInt(process.env.count) or 6000
duration =parseInt(process.env.duration) or 30
balance = process.env.host or "http://10.160.10.195:3014"
base = parseInt(process.env.base) or 0

diffArray = []
map = {}
max = 0


timer = null


connectedCount = 0
dataCount = 0
HRCount = 0
DRCount = 0
OKCount = 0
CLCount = 0

setInterval ->
  console.log connectedCount, "HRCount:#{HRCount}, DRCount:#{DRCount}, OKCount:#{OKCount}, CLCount:#{CLCount}"
,3000

go = (shopId)->
  timer2 = null
  request("#{balance}/#{shopId}").end((err, res)->
    # console.log "#{res.body.host}:#{res.body.clientPort}"
    if err 
      console.log shopId, err
      return 

    client = net.connect(
      port: res.body.clientPort
      host: res.body.host
    , -> #'connect' listener
      client.write "DP:#{shopId}.05&"
      connectedCount++
      
      timer2 = setInterval ->
        client.write "HP:#{shopId}.C.05&"
      ,8000

    )

    client.on "data", (chunk)->
      dataCount++
      
      command = chunk.toString("utf8", 0, 2)
      switch command
        when "HR"
          HRCount++
        when "DR"
          DRCount++
        when "OK"
          OKCount++
        when "CL"
          CLCount++


    client.on "close", ->
      console.log "#{shopId}close"
      connectedCount--

      clearInterval(timer2)
      client.destroy()
      client.unref()

    client.on "error", ->
      client.destroy()
      client.unref()

  )

lazyGo = (shopId, time)->
  setTimeout ->
    go(shopId)
  , time

baseIndex = 100000 + base*count+1
for i in [0...count]
  index = i + baseIndex
  lazyGo("S#{index}", duration*1000/count*i)


