
net = require("net")
request = require("superagent")

balance = process.env.host or "http://121.40.115.13:3014"
shopId = shopId = process.env.shopId or "S100010"

diffArray = []
map = {}
max = 0


timer = null


connectedCount = 0
dataCount = 0
HRCount = 0
DRCount = 0

OKCount = 0
CLCount = 0

log = ->
  console.log new Date(), Array.prototype.slice.call(arguments)

setInterval ->
  log connectedCount, "HRCount:#{HRCount}, DRCount:#{DRCount}, OKCount:#{OKCount}, CLCount:#{CLCount}"
,3000



go = (shopId)->
  timer2 = null
  request("#{balance}/#{shopId}").end((err, res)->
    # log "#{res.body.host}:#{res.body.clientPort}"
    if err 
      log shopId, err
      return 

    client = net.connect(
      port: res.body.clientPort
      host: res.body.host
    , -> #'connect' listener
      client.write "DP:#{shopId}&"
      connectedCount++
      
      timer2 = setInterval ->
        client.write "HP:#{shopId}.C.05&"
      ,8000

    )

    client.on "data", (chunk)->
      log chunk.toString("utf8")
      dataCount++
      
      command = chunk.toString("utf8", 0, 2)
      switch command
        when "HR"
          HRCount++
        when "DR"
          DRCount++
        when "OK"
          OKCount++
        when "CL"
          CLCount++


    client.on "close", ->
      log "#{shopId}close"
      connectedCount--

      clearInterval(timer2)
      client.destroy()
      client.unref()

    client.on "error", ->
      client.destroy()
      client.unref()

  )

go(shopId)